package xcommand.plantuml.core

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xtool.core.command.XctlCommand

@Component
@Command(
    name = "plantuml",
    mixinStandardHelpOptions = true
)
internal class PlantumlRootCommand() : XctlCommand() {
    override fun run() {
        TODO("Not yet implemented")
    }
}
