package xcommand.plantuml.core

import org.springframework.context.annotation.Configuration
import picocli.CommandLine
import xtool.core.command.XctlCommand
import xtool.core.config.XctlCommandRegister

@Configuration
class PlantumlCommandRegister(
    private val factory: CommandLine.IFactory,
    private val rootCommand: XctlCommand,
    private val subcommands: List<PlantumlCommand>
) : XctlCommandRegister {
    override fun register(commandLine: CommandLine) {
        val root = CommandLine(rootCommand, factory)
        subcommands.forEach { root.addSubcommand(it) }
        commandLine.addSubcommand(root)
    }


}
