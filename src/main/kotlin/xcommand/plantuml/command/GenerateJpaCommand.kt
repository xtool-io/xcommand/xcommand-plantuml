package xcommand.plantuml.command

import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import picocli.CommandLine.Command
import xcommand.plantuml.core.PlantumlCommand

@Configuration
@Command(
    name = "generate-jpa",
    mixinStandardHelpOptions = true
)
class GenerateJpaCommand(private val env: Environment) : PlantumlCommand() {
    override fun run() {
        println("Hello: ${env.activeProfiles}")
    }
}
